GeneFlow App for Definition Validation
======================================

Version: 0.1

This is a GeneFlow app used for definition validation.

Inputs
------

1. file: List all app inputs and their descriptions.

Parameters
----------

1. output: List all app parameters and their descriptions.

